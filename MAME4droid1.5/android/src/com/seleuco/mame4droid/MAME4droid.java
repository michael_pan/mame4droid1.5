/*
 * This file is part of MAME4droid.
 *
 * Copyright (C) 2013 David Valdeita (Seleuco)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Linking MAME4droid statically or dynamically with other modules is
 * making a combined work based on MAME4droid. Thus, the terms and
 * conditions of the GNU General Public License cover the whole
 * combination.
 *
 * In addition, as a special exception, the copyright holders of MAME4droid
 * give you permission to combine MAME4droid with free software programs
 * or libraries that are released under the GNU LGPL and with code included
 * in the standard release of MAME under the MAME License (or modified
 * versions of such code, with unchanged license). You may copy and
 * distribute such a system following the terms of the GNU GPL for MAME4droid
 * and the licenses of the other code concerned, provided that you include
 * the source code of that other code when and as the GNU GPL requires
 * distribution of source code.
 *
 * Note that people who make modified versions of MAME4idroid are not
 * obligated to grant this special exception for their modified versions; it
 * is their choice whether to do so. The GNU General Public License
 * gives permission to release a modified version without this exception;
 * this exception also makes it possible to release a modified version
 * which carries forward this exception.
 *
 * MAME4droid is dual-licensed: Alternatively, you can license MAME4droid
 * under a MAME license, as set out in http://mamedev.org/
 */

package com.seleuco.mame4droid;

import java.io.File;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.seleuco.mame4droid.helpers.MainHelper;
import com.seleuco.mame4droid.helpers.PrefsHelper;
import com.seleuco.mame4droid.input.ControlCustomizer;
import com.seleuco.mame4droid.input.InputHandler;
import com.seleuco.mame4droid.input.InputHandlerFactory;
import com.seleuco.mame4droid.views.FilterView;
import com.seleuco.mame4droid.views.IEmuView;
import com.seleuco.mame4droid.views.InputView;
import com.seleuco.mame4droid139.R;

public class MAME4droid extends Activity {

	protected View emuView = null;

	protected InputView inputView = null;

	protected FilterView filterView = null;

	protected MainHelper mainHelper = null;
	protected PrefsHelper prefsHelper = null;
	protected InputHandler inputHandler = null;

	public PrefsHelper getPrefsHelper() {
		return prefsHelper;
	}

	public MainHelper getMainHelper() {
		return mainHelper;
	}

	public View getEmuView() {
		return emuView;
	}

	public InputView getInputView() {
		return inputView;
	}

	public FilterView getFilterView() {
		return filterView;
	}

	public InputHandler getInputHandler() {
		return inputHandler;
	}

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Log.d("EMULATOR", "onCreate " + this);

		overridePendingTransition(0, 0);
		getWindow().setWindowAnimations(0);

		prefsHelper = new PrefsHelper(this);

		mainHelper = new MainHelper(this);

		inputHandler = InputHandlerFactory.createInputHandler(this);

		mainHelper.detectOUYA();

		Emulator.setPortraitFull(getPrefsHelper().isPortraitFullscreen());
		boolean full = false;
		if (prefsHelper.isPortraitFullscreen()
				&& mainHelper.getscrOrientation() == Configuration.ORIENTATION_PORTRAIT) {
			setContentView(R.layout.main_fullscreen);
			full = true;
		} else {
			setContentView(R.layout.main);
		}

		FrameLayout fl = (FrameLayout) this.findViewById(R.id.EmulatorFrame);

		// Coment to avoid BUG on 2.3.4 (reload instead)
		Emulator.setVideoRenderMode(prefsHelper.getVideoRenderMode());

		if (prefsHelper.getVideoRenderMode() == PrefsHelper.PREF_RENDER_SW) {
			this.getLayoutInflater().inflate(R.layout.emuview_sw, fl);
			emuView = this.findViewById(R.id.EmulatorViewSW);
		}

		else {
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
				this.getLayoutInflater().inflate(R.layout.emuview_gl_ext, fl);
			else
				this.getLayoutInflater().inflate(R.layout.emuview_gl, fl);

			emuView = this.findViewById(R.id.EmulatorViewGL);
		}

		if (full && prefsHelper.isPortraitTouchController()) {
			FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) emuView
					.getLayoutParams();
			lp.gravity = Gravity.TOP | Gravity.CENTER;
		}

		inputView = (InputView) this.findViewById(R.id.InputView);

		((IEmuView) emuView).setMAME4droid(this);

		inputView.setMAME4droid(this);

		Emulator.setMAME4droid(this);

		View frame = this.findViewById(R.id.EmulatorFrame);
		frame.setOnTouchListener(inputHandler);

		if ((prefsHelper.getPortraitOverlayFilterValue() != PrefsHelper.PREF_OVERLAY_NONE && mainHelper
				.getscrOrientation() == Configuration.ORIENTATION_PORTRAIT)
				|| (prefsHelper.getLandscapeOverlayFilterValue() != PrefsHelper.PREF_OVERLAY_NONE && mainHelper
						.getscrOrientation() == Configuration.ORIENTATION_LANDSCAPE)) {
			String value;

			if (mainHelper.getscrOrientation() == Configuration.ORIENTATION_PORTRAIT)
				value = prefsHelper.getPortraitOverlayFilterValue();
			else
				value = prefsHelper.getLandscapeOverlayFilterValue();

			if (!value.equals(PrefsHelper.PREF_OVERLAY_NONE)) {
				getLayoutInflater().inflate(R.layout.filterview, fl);
				filterView = (FilterView) this.findViewById(R.id.FilterView);

				String fileName = getPrefsHelper().getROMsDIR()
						+ File.separator + "overlays" + File.separator + value;

				Bitmap bmp = BitmapFactory.decodeFile(fileName);
				BitmapDrawable bitmapDrawable = new BitmapDrawable(bmp);
				bitmapDrawable.setTileModeXY(Shader.TileMode.REPEAT,
						Shader.TileMode.REPEAT);

				int alpha = 0;
				switch (getPrefsHelper().getEffectOverlayIntensity()) {
				case 1:
					alpha = 25;
					break;
				case 2:
					alpha = 50;
					break;
				case 3:
					alpha = 55;
					break;
				case 4:
					alpha = 60;
					break;
				case 5:
					alpha = 65;
					break;
				case 6:
					alpha = 70;
					break;
				case 7:
					alpha = 75;
					break;
				case 8:
					alpha = 80;
					break;
				case 9:
					alpha = 100;
					break;
				case 10:
					alpha = 125;
					break;
				}

				bitmapDrawable.setAlpha(alpha);
				filterView.setBackgroundDrawable(bitmapDrawable);

				// this.getEmuView().setAlpha(250);

				if (full && prefsHelper.isPortraitTouchController()) {
					FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) filterView
							.getLayoutParams();
					lp.gravity = Gravity.TOP | Gravity.CENTER;
				}

				filterView.setMAME4droid(this);
			}

		}

		inputHandler.setInputListeners();

		mainHelper.updateMAME4droid();
		Intent intent = getIntent();
		String action = intent.getAction();
		if (action.equals(Intent.ACTION_VIEW)) {
			Log.e("test", "action = " + action);
			Uri data = intent.getData();
			Log.e("test", "data = " + data);
			if (data != null) {
				String romname = data.getLastPathSegment();
				Log.e("test", "romname = " + romname);
				romname = romname.replaceAll(".zip", "");
				Log.e("test", "romname = " + romname);
				runMAME4droid(romname);
			}
		} else {
			runMAME4droid("mslug5");
		}
	}

	public void runMAME4droid(String romname) {
		getMainHelper().copyFiles();
		Emulator.emulate(mainHelper.getLibDir(),
				mainHelper.getDefaultROMsDIR(), romname);
	}

	// ACTIVITY
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (mainHelper != null)
			mainHelper.activityResult(requestCode, resultCode, data);
	}

	// LIVE CYCLE
	@Override
	protected void onResume() {
		Log.d("EMULATOR", "onResume");
		super.onResume();
		if (prefsHelper != null)
			prefsHelper.resume();

		else if (!ControlCustomizer.isEnabled())
			Emulator.resume();

		if (inputHandler != null) {
			if (inputHandler.getTiltSensor() != null)
				inputHandler.getTiltSensor().enable();
		}

	}

	@Override
	protected void onPause() {
		Log.d("EMULATOR", "onPause");
		super.onPause();
		if (prefsHelper != null)
			prefsHelper.pause();
		if (!ControlCustomizer.isEnabled())
			Emulator.pause();
		if (inputHandler != null) {
			if (inputHandler.getTiltSensor() != null)
				inputHandler.getTiltSensor().disable();
		}
	}

	@Override
	protected void onStart() {
		Log.d("EMULATOR", "onStart");
		super.onStart();
		// System.out.println("OnStart");
	}

	@Override
	protected void onStop() {
		Log.d("EMULATOR", "onStop");
		super.onStop();
		// System.out.println("OnStop");
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		Log.d("EMULATOR", "onNewIntent");
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		Log.d("EMULATOR", "onDestroy " + this);

		View frame = this.findViewById(R.id.EmulatorFrame);
		if (frame != null)
			frame.setOnTouchListener(null);

		if (inputHandler != null) {
			inputHandler.unsetInputListeners();

			if (inputHandler.getTiltSensor() != null)
				inputHandler.getTiltSensor().disable();
		}

		if (emuView != null)
			((IEmuView) emuView).setMAME4droid(null);

		/*
		 * if(inputView!=null) inputView.setMAME4droid(null);
		 * 
		 * if(filterView!=null) filterView.setMAME4droid(null);
		 * 
		 * prefsHelper = null;
		 * 
		 * dialogHelper = null;
		 * 
		 * mainHelper = null;
		 * 
		 * fileExplore = null;
		 * 
		 * menuHelper = null;
		 * 
		 * inputHandler = null;
		 * 
		 * inputView = null;
		 * 
		 * emuView = null;
		 * 
		 * filterView = null;
		 */
	}

	private static final long PRESS_BACK_TIME = 2 * 1000;
	private long lastPressBackTime = 0;

	@Override
	public void onBackPressed() {
		long currentPressBackTime = System.currentTimeMillis();
		if (currentPressBackTime - lastPressBackTime < PRESS_BACK_TIME) {
			Emulator.resume();
			Emulator.setValue(Emulator.EXIT_GAME_KEY, 1);
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			Emulator.setValue(Emulator.EXIT_GAME_KEY, 0);
			finish();

		} else {
			Toast toast = Toast.makeText(this, R.string.toast_exit,
					Toast.LENGTH_SHORT);
			toast.show();
		}
		lastPressBackTime = currentPressBackTime;
	}

}