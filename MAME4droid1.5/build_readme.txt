MAME1.5编译过程记录：
1. 安装 android-ndk-r9d  android-adt-eclipse
   sudo apt-get install ia32-libs  //运行aapt和adb需要
   sudo apt-get install gnome-tweak-tool  //改变ubuntu字体
   sudo apt-get install aria2      //下载工具
   sudo apt-get install google-chrome-stable  
   sudo apt-get install git-core
   sudo apt-get install build-essential make libc6-dev
   sudo apt-get install bison patch texinfo libncurses-dev
   sudo apt-get install gnupg  ncurses-dev zlib1g-dev valgrind 
   sudo apt-get install mercurial
   sudo apt-get install tree
   sudo apt-get install bison gawk gcc libc6-dev make
   sudo apt-get install ibus-googlepinyin
   sudo apt-get install p7zip-full 


2. 创建 standalone toolchain 
   #cd ~/android-ndk-r9d/build/tools/
   ~/android-ndk-r9d/build/tools# ./make-standalone-toolchain.sh --platform=android-19 --install-dir=/home/panyingyun/android/ --ndk-dir=/home/panyingyun/android-ndk-r9d/ --system=linux-x86_64
Auto-config: --toolchain=arm-linux-androideabi-4.6
Copying prebuilt binaries...
Copying sysroot headers and libraries...
Copying libstdc++ headers and libraries...
Copying files to: /home/panyingyun/android/
Cleaning up...
Done.

OK 创建toolchain成功了，下面我们就可以用他来编译MAME1.5的src

3. 因为要编译C++代码,并且需要链接libstdc++.a，所以我们需要创建独立的toolchain 

4. 编译之前我们需要先编译官方mame.139u的源码

5. 添加到环境变量或者直接写全路径
6. 回到/home/panyingyun中
   vim .profile
我的环境变量是这样设置的

#set liteIDE(Golang IDE)
export PATH="$HOME/liteide/bin:$PATH"
#set android ndk/sdk/ide
export PATH="$HOME/android-ndk-r9d:$HOME/android-ndk-r9d/build/tools;$PATH"
export PATH="$HOME/adt/eclipse:$HOME/android-studio/bin:$PATH"
export PATH="$HOME/adt/sdk/build-tools/android-4.4.2:$PATH"
export PATH="$HOME/adt/sdk/platform-tools:$PATH"
export PATH="$HOME/adt/sdk/tools:$PATH"
#set java path
export JAVA_HOME=$HOME/jdk
export CLASSPATH=$HOME/jdk/lib
export PATH="${JAVA_HOME}/bin:$CLASSPATH:$PATH"
#set go path
export GOPATH=$HOME/gopro/
export GOBIN=$HOME/gopro/bin/
#set arm-linux-gcc path
export ARM_GCC_BIN=$HOME/android/bin
export ARM_GCC_SYSROOT=$HOME/android/sysroot
export PATH="${ARM_GCC_BIN}:$PATH"

7.  编辑makefile 设置好路径后，编译。
出现 https://code.google.com/p/imame4all/issues/detail?id=261，很多***找不的之来的错误

etc.

It is caused by the fact that 'Mame' has the same name with 'memory.h' in 'NDK'. It needs to rename 'sysroot/usr/include/memory.h', e.g., 'NDK/memorys.h'. And then, revise 'memory.h' to be 'memorys.h' in 'sysroot/usr/include/stdlib.h'.  I found that this file is used in one place only.

修改 toolchain下面的memory.h 变成 memorys.h 另外stdlib.h引用的地方也该下即可。
#] cd /home/panyingyun/android/sysroot/usr/include
#] mv memory.h memorys.h
#] vim stdlib.h 将<memory.h>头文件引入该为<memorys.h>

OK 又出现新的错误,这时需要 prec-build/file2str
pre-build下面的几个文件是在gcc下面编译的 而不是在arm-linux-androideabi-gcc下编译的，
其实这几个文件在makfile中是涉及到的，但是对makefile不是说非常了解。但是我有自己的解决办法

那就就去下载mame0.139u1 PC源码，通过它编译这些  prec-build/ 下的文件

8. 下载 mame0.139u1 PC源码 (http://mamedev.org/)
    http://www.mamedev.org/oldrel.html
    下载版本0.139, http://www.mamedev.org/downloader.php?file=releases/mame0139s.zip
    大小 16.7 MB 
9.  sudo apt-get install libgtk2.0-dev 否则编译mame0.139会出现错误
10. 解压 7z x mame0139s.zip 
    make 即可
11. 出现
    错误： variable ‘id’ set but not used 
    修改makefile，去掉 -Werror 即可
    这时会出现其他错误，我们暂时先不要管这个编译错误了（makefile真的不是低级程序员玩的）
    因为我们要的文件已经齐全了，在哪里呢？在这里mame0139/obj/sdl/mame64/build
    file2str  m68kmake  png2bdc   tmsmake  verinfo
    OK 那边我们拷贝这几个文件到之前的MAME1.5项目中. 
    具体步骤：在MAME4droid1.5下面建立一个prec-build目录：

panyingyun@T440p:~/mame4droid1.5/MAME4droid1.5$ tree -L 1
.
├── android
├── makefile
├── MAME4droid_0.139u1_ROMs+extras.torrent
├── obj
├── prec-build
└── src
 目录结构大概是这样的
  拷贝 file2str  m68kmake  png2bdc   tmsmake  verinfo 到这个pre-build目录下面
  chmod 440 file2str  m68kmake  png2bdc   tmsmake  verinfo
我是直接拷贝到obj/droid-ios/mame64/build/,因为makefile也是需要做这个拷贝的
#] cp pre-build/file2str obj/droid-ios/mame64/build/

12. make 出现下面错误
src/emu/validity.c:33:57: error: size of array 'your_ptr64_flag_is_wrong' is negative
make: *** [obj/droid-ios/mame64/emu/validity.o] 错误 1
 修改makefile的 
 523 # define PTR64 if we are a 64-bit target
 524 ifeq ($(PTR64),1)
 525 #DEFS += -DPTR64  //注释掉这一行就OK了
 526 endif

 make 就能编译出来so了




   
 





